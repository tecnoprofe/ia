import tensorflow as tf

#Activación para sesiones de evaluación
tf.compat.v1.disable_eager_execution()

#Declaración de constantes
valor1 = tf.constant(2)
valor2 = tf.constant(3)

#Suma de constantes
suma=valor1+valor2
print("la suma es",suma)

# Iniciar una sesión METODO 1
# sesion = tf.compat.v1.Session()
# print("la suma es ",sesion.run(suma))

#Iniciar una sesión METODO 2
with tf.compat.v1.Session() as sesion:
    print("El resultado es:",sesion.run(suma))
