#suma y multiplicación de tensores
import tensorflow as tf
with tf.compat.v1.Session() as sesion:
    a = tf.constant([[2,2],[1,4]])
    b = tf.constant([[1,3],[5,2]])
    suma = tf.add(a,b)
    multiplicacion = tf.matmul(a,b)*2

    print("El resultado de la suma es: ",sesion.run(suma))
    print("El resultado de la multiplicación es: ",sesion.run(multiplicacion))
    sesion.close()
