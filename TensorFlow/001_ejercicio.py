import tensorflow as tf
# compatibilidad entre la librería de Tensorflow con python
tf.compat.v1.disable_eager_execution()

#__>>>>> DECLARACIÓN DE VARIABLES Y OPERACIONES.
#_______________________________________________
#Creación de una constante
saludo = tf.constant('Hola Mundo')
print(saludo)

#__>>>>> EJECUCIÓN DEL GRAFO
#___________________________
#Declaración de una Sesión
sesion = tf.compat.v1.Session()
#Imprimir el resultado
print(sesion.run(saludo))