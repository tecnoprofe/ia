# importamos la libreria
import tensorflow as tf

# importamos librerías adicionales
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

tf.compat.v1.disable_eager_execution()
# Creación de Constantes
# El valor que retorna el constructor es el valor de la constante.

# creamos constantes a=2 y b=3
a = tf.constant(2)
b = tf.constant(3)

# creamos matrices de 3x3
matriz1 = tf.constant([[1, 3, 2],
                       [1, 0, 0],
                       [1, 2, 2]])

matriz2 = tf.constant([[1, 0, 5],
                       [7, 5, 0],
                       [2, 1, 1]])

# Realizamos algunos cálculos con estas constantes
suma = tf.add(a, b)
mult = a*b
cubo_a = a**3

# suma de matrices
suma_mat = tf.add(matriz1, matriz2)

# producto de matrices
mult_mat = tf.matmul(matriz1, matriz2)

# Todo en TensorFlow ocurre dentro de una Sesión

# creamos la sesion y realizamos algunas operaciones con las constantes
# y lanzamos la sesión
with tf.compat.v1.sesionion() as sesion:

    print("Suma de las constantes: {}".format(sesion.run(suma)))
    print("Multiplicación de las constantes: {}".format(sesion.run(mult)))
    print("Constante elevada al cubo: {}".format(sesion.run(cubo_a)))
    print("Suma de matrices: \n{}".format(sesion.run(suma_mat)))
    print("Producto de matrices: \n{}".format(sesion.run(mult_mat)))