# GRAFICAR LA FUNCIÓN DE LA TANGENTE
# CALCULAR LA DERIVADA. 

#De numpy se importa sin, linspace y power
from numpy import sin,linspace,power
#De pylab se importa plot y show
from pylab import plot,show

#Se define la funcion de la curva
def f(x): # sample function
    return x**2

# Se evalua la funcion en el rango de -2 a 4 con 150 intervalos
x = linspace(-7,8,100)
#Se le pasa a la funcion los valores definidos en x.
y = f(x)

#Se le pasa el punto a calcular la tangente de la curva.
a = 3
#Se le pasa el diferencial con un valor de 0.1
h = 0.001
#Se calcula la derivada.
fprime = (f(a+h)-f(a))/h # derivada
#Se calcula la tangente
tan = f(a)+fprime*(x-a)  # tangente

# Se grafica la funcion y la tangente.
plot(x,y,'b',a,f(a),'om',x,tan,'--r')
#Se muestra la grafica.

show()