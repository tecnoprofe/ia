#DERIVADAS
import sympy
#Declaramos los símbolos que usaremos
x = sympy.symbols('x')
gx=sympy.diff(x**2)
print(gx)
#Derivada de un función trigonométrica, como cos(x)
gx=sympy.diff(sympy.cos(x), x)
print(gx)
#Derivada de una potencia
gx=sympy.diff(5/x**5, x)
print(gx)
