import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QGridLayout, QLabel, QVBoxLayout
from PyQt5.QtCore import Qt, QTimer
from collections import deque
import numpy as np

class PuzzleWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.grid_size = 3
        self.labels = {}
        for i in range(9):
            label = QLabel(str(i+1) if i < 8 else '')
            label.setAlignment(Qt.AlignCenter)
            label.setStyleSheet("font-size: 20px; border: 1px solid black; min-width: 60px; min-height: 60px;")
            self.labels[i] = label
        # Inicializamos positions aquí para asegurar que siempre esté definido antes de usarlo.
        self.positions = np.random.permutation(9)
        self.initUI()
        self.shuffle()

    def initUI(self):
        self.grid = QGridLayout(self)
        self.update_grid()
        self.setLayout(self.grid)
        self.setWindowTitle('8 Puzzle Solver')
        self.setGeometry(300, 300, 180, 200)

    def shuffle(self):
        np.random.shuffle(self.positions)
        self.update_grid()

    def update_grid(self):
        for index, label in self.labels.items():
            row = self.positions[index] // self.grid_size
            col = self.positions[index] % self.grid_size
            self.grid.addWidget(label, row, col)

    def bfs_solve(self):
        start_state = tuple(self.positions)
        goal_state = tuple(range(9))
        queue = deque([start_state])
        paths = {start_state: []}
        visited = set([start_state])

        while queue:
            current = queue.popleft()
            if current == goal_state:
                self.display_solution(paths[current])
                return

            idx = current.index(8)  # Empty tile represented by 8
            x, y = divmod(idx, self.grid_size)

            for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                nx, ny = x + dx, y + dy
                if 0 <= nx < self.grid_size and 0 <= ny < self.grid_size:
                    n_idx = nx * self.grid_size + ny
                    new_positions = list(current)
                    new_positions[idx], new_positions[n_idx] = new_positions[n_idx], new_positions[idx]
                    new_state = tuple(new_positions)

                    if new_state not in visited:
                        visited.add(new_state)
                        queue.append(new_state)
                        paths[new_state] = paths[current] + [new_state]

    def display_solution(self, solution):
        self.solution_iter = iter(solution)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.show_next_step)
        self.timer.start(500)

    def show_next_step(self):
        try:
            state = next(self.solution_iter)
            self.positions = state
            self.update_grid()
        except StopIteration:
            self.timer.stop()

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.puzzle = PuzzleWidget()
        self.btn = QPushButton('Solve Puzzle', self)
        self.btn.clicked.connect(self.puzzle.bfs_solve)
        layout = QVBoxLayout(self)
        layout.addWidget(self.puzzle)
        layout.addWidget(self.btn)
        self.setLayout(layout)
        self.setWindowTitle('Puzzle Solver')
        self.setGeometry(300, 300, 200, 250)
        self.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())
