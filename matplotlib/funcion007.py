# Jaime Zambrana Chacón.
# Mis redes sociales
# https://www.youtube.com/tecnoprofe
# https://www.facebook.com/zambranachaconjaime
# https://gitlab.com/tecnoprofe
# https://www.tiktok.com/@jaimezambranachacon?lang=es
# <<<=============================================>>>
# <<<========= FUNCION CUBICA ========>>>
import numpy as np
import matplotlib.pyplot as plt
import math
X=np.arange(-7,7,1,dtype=np.float64)
Y=X*X*X
#X*X*X es equivalente a X^3
print("vector de coordenadas X =",X)
print("vector de coordenadas Y =",Y)

#Impresion de graficas

#Los metodos axhline y axvline generan las lineas en forma de cruz
plt.axhline(0, color="#BBCCDD")
plt.axvline(0, color="#BBCCDD")
plt.plot(X,Y,'bo-')
plt.title("y=x^3")
plt.ylabel("Coordenada y")
plt.xlabel("Coordenada x")
plt.grid()
plt.show()
