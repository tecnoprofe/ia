# Jaime Zambrana Chacón.
# Mis redes sociales
# https://www.youtube.com/tecnoprofe
# https://www.facebook.com/zambranachaconjaime
# https://gitlab.com/tecnoprofe
# https://www.tiktok.com/@jaimezambranachacon?lang=es
# <<<=============================================>>>
# <<<========= Funcion y=x^2 ========>>>

import numpy as np
import matplotlib.pyplot as plt
import math
ejex=np.arange(-100,100,1,dtype=np.int64)
dimension=len(ejex)
ejey=np.zeros(dimension,dtype=np.int64)
print("Dimensión del ejex =",dimension)
print("valores del ejex =",ejex)
for i in range(dimension):
    # y=x^2
    ejey[i]=math.pow(ejex[i],2)
print("valores del ejey =",ejey)
plt.plot(ejex,ejey)
plt.show()