import matplotlib.pyplot as plt
import numpy as np
ejex= np.array([0,1,2,3,4,5])
ejey= np.array([3,4,5,6,7,8])
plt.plot(ejex,ejey,'bo-')
plt.title("f(x)=x+3")
plt.show()
