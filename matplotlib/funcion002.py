# Jaime Zambrana Chacón.
# Mis redes sociales
# https://www.youtube.com/tecnoprofe
# https://www.facebook.com/zambranachaconjaime
# https://gitlab.com/tecnoprofe
# https://www.tiktok.com/@jaimezambranachacon?lang=es
# <<<=============================================>>>
# <<<========= Funcion y=x^2 ========>>>

import numpy as np
import matplotlib.pyplot as plt
import math

x = np.linspace(-np.pi, np.pi,1000)
y = 3*np.cos(2*x)
plt.plot(x,y)
plt.show()