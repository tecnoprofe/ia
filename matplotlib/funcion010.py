# Jaime Zambrana Chacón.
# Mis redes sociales
# https://www.youtube.com/tecnoprofe
# https://www.facebook.com/zambranachaconjaime
# https://gitlab.com/tecnoprofe
# https://www.tiktok.com/@jaimezambranachacon?lang=es
# <<<=============================================>>>
# <<<========= Función Raíz Cúbica.  =========>>>
import numpy as np
import matplotlib.pyplot as plt
import math
X=np.arange(-100,101,1,dtype=np.float64)
Y=[math.sqrt(x) for i ]
#X*X*X es equivalente a X^3
print("vector de coordenadas X =",X)
print("vector de coordenadas Y =",Y)

#Impresion de graficas

#Los metodos axhline y axvline generan las lineas en forma de cruz
plt.axhline(0, color="#333333")
plt.axvline(0, color="#333333")
plt.plot(X,Y,'b-')
plt.title("Función Racional. y=1/x")
plt.ylabel("Coordenada y")
plt.xlabel("Coordenada x")
plt.grid()
plt.show()
