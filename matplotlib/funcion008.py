# Jaime Zambrana Chacón.
# Mis redes sociales
# https://www.youtube.com/tecnoprofe
# https://www.facebook.com/zambranachaconjaime
# https://gitlab.com/tecnoprofe
# https://www.tiktok.com/@jaimezambranachacon?lang=es
# <<<=============================================>>>
# <<<========= FUNCION ABSOLUTA ========>>>
import numpy as np
import matplotlib.pyplot as plt
import math
X=np.arange(-10,11,1,dtype=np.float64)
Y=abs(X)

print("vector de coordenadas X =",X)
print("vector de coordenadas Y =",Y)

#Impresion de graficas

#Los metodos axhline y axvline generan las lineas en forma de cruz
plt.axhline(0, color="#BBCCDD")
plt.axvline(0, color="#BBCCDD")
plt.plot(X,Y,'bo-')
plt.title("y=|x|")
plt.ylabel("Coordenada y")
plt.xlabel("Coordenada x")
plt.grid()
plt.show()
