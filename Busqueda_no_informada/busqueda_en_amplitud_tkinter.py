from Arbol import Nodo
def busqueda_amplitud(e_inicial,e_final):
    exito=False
    nodos_frontera=[]
    nodos_visitados=[]
    # Creación del primer nodo inicial o un ojbeto de tipo Nodo
    NodoInicial=Nodo(e_inicial)
    nodos_frontera.append(NodoInicial)
    comparaciones=1
    while len(nodos_frontera)!=0 and (not exito):
        #nodo es el Nodo padre inicialmente
        nodo=nodos_frontera.pop(0)
        nodos_visitados.append(nodo)

        if nodo.obtener_datos()==e_final:
            exito=True
            print("cantidad de comparaciones: ",comparaciones)
            return nodo
        else:
            comparaciones+=1
            # auxiliar es una lista con el nodo padre
            #hijo izquierdo
            aux=nodo.obtener_datos()
            auxiliar_hijo=[aux[1],aux[0],aux[2],aux[3]]
            hijo_izquierdo=Nodo(auxiliar_hijo)

            if not hijo_izquierdo.en_lista(nodos_visitados) and not hijo_izquierdo.en_lista(nodos_frontera):
                nodos_frontera.append(hijo_izquierdo)

            #hijo central
            aux=nodo.obtener_datos()
            auxiliar_hijo=[aux[0],aux[2],aux[1],aux[3]]
            hijo_central=Nodo(auxiliar_hijo)

            if not hijo_central.en_lista(nodos_visitados) and not hijo_central.en_lista(nodos_frontera):
                nodos_frontera.append(hijo_central)

            #hijo derecho
            aux=nodo.obtener_datos()
            auxiliar_hijo=[aux[0],aux[1],aux[3],aux[2]]
            hijo_derecho=Nodo(auxiliar_hijo)
            
            if not hijo_derecho.en_lista(nodos_visitados) and not hijo_derecho.en_lista(nodos_frontera):
                nodos_frontera.append(hijo_derecho)
            
            #asingar hijos a su padre
            nodo.asignar_hijos([hijo_izquierdo,hijo_central,hijo_derecho])






import tkinter as tk
ventana = tk.Tk()

entrada1= tk.Entry(ventana,width=3)
entrada1.place(x=10, y=50)

entrada2= tk.Entry(ventana,width=3)
entrada2.place(x=30, y=50)

entrada3= tk.Entry(ventana,width=3)
entrada3.place(x=50, y=50)

entrada4= tk.Entry(ventana,width=3)
entrada4.place(x=70, y=50)

def entradapuzle():
    estado_inicial=[entrada1.get(),entrada2.get(),entrada3.get(),entrada4.get()]    
    estado_final=['2','1','3','4']
    solucion=busqueda_amplitud(estado_inicial,estado_final)
    print("La solución es.: ",solucion)
    nivel=0

    datos_camino=[]
    nodo_aux=solucion
    while(nodo_aux.obtener_padre()!=None):
        datos_camino.append(nodo_aux.obtener_datos())
        nodo_aux=nodo_aux.obtener_padre()
    datos_camino.append(estado_inicial)
    datos_camino.reverse()

    Lbl_resultado=tk.Label(ventana, text="Resultado")
    Lbl_resultado.place(width=700,x=100, y=100)
    Lbl_resultado.config(text=datos_camino)
    

boton1=tk.Button(ventana,text="Puzzle lineal.",command=entradapuzle)
boton1.place(width=100,x=100, y=50)


ventana.mainloop()



