from heapq import heappop, heappush

class Puzzle:
    def __init__(self, initial, parent=None, action=None):
        self.state = initial
        self.parent = parent
        self.action = action
        self.path_cost = 0 if parent is None else parent.path_cost + 1
        self.heuristic = self._compute_heuristic()
        self.priority = self.path_cost + self.heuristic

    def _compute_heuristic(self):
        # Distancia de Manhattan
        h = 0
        for i in range(3):
            for j in range(3):
                if self.state[i][j] != 0:
                    x, y = divmod(self.state[i][j] - 1, 3)
                    h += abs(x - i) + abs(y - j)
        return h

    def __lt__(self, other):
        return self.priority < other.priority

    def _swap_blank(self, x1, y1, x2, y2):
        copied = [row[:] for row in self.state]
        copied[x1][y1], copied[x2][y2] = copied[x2][y2], copied[x1][y1]
        return copied

    def get_neighbors(self):
        neighbors = []
        x, y = None, None
        for i in range(3):
            for j in range(3):
                if self.state[i][j] == 0:
                    x, y = i, j
                    break

        for dx, dy, action in [(-1, 0, 'UP'), (1, 0, 'DOWN'), (0, -1, 'LEFT'), (0, 1, 'RIGHT')]:
            newx, newy = x + dx, y + dy
            if 0 <= newx < 3 and 0 <= newy < 3:
                neighbors.append(Puzzle(self._swap_blank(x, y, newx, newy), self, action))
        return neighbors

def a_star_search(initial):
    goal = [[1, 2, 3], [4, 5, 6], [7, 8, 0]]
    open_list = [Puzzle(initial)]
    closed_list = set()

    while open_list:
        current = heappop(open_list)

        if current.state == goal:
            actions = []
            while current.parent:
                actions.append(current.action)
                current = current.parent
            actions.reverse()
            return actions

        closed_list.add(tuple(map(tuple, current.state)))

        for neighbor in current.get_neighbors():
            if tuple(map(tuple, neighbor.state)) not in closed_list:
                heappush(open_list, neighbor)

    return None

initial = [[1, 2, 3], 
           [4, 5, 6], 
           [7, 0, 8]]
solution = a_star_search(initial)
print(solution)
