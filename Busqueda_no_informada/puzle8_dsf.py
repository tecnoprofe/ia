from collections import deque

class Puzzle8:
    def __init__(self, initial_state):
        self.initial_state = initial_state

    def get_zero_position(self, state):
        for i in range(3):
            for j in range(3):
                if state[i][j] == 0:
                    return (i, j)

    def moves(self, state):
        i, j = self.get_zero_position(state)
        new_states = []
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        for di, dj in directions:
            ni, nj = i + di, j + dj
            if 0 <= ni < 3 and 0 <= nj < 3:
                new_state = [row.copy() for row in state]
                new_state[i][j], new_state[ni][nj] = new_state[ni][nj], new_state[i][j]
                new_states.append(new_state)
        return new_states

    def bfs(self, goal_state):
        visited = set()
        queue = deque([(self.initial_state, [])])
        
        while queue:
            current_state, path = queue.popleft()

            if current_state == goal_state:
                return path + [current_state]  # Retornamos el camino completo al estado objetivo

            visited.add(tuple(map(tuple, current_state)))
            for next_state in self.moves(current_state):
                if tuple(map(tuple, next_state)) not in visited:
                    queue.append((next_state, path + [current_state]))

        return None  # No se encontró solución

import time 
start_time = time.time()  # Guarda el tiempo inicial


# Uso:
initial = [[8, 7, 6],
           [5, 4, 3], 
           [2, 1, 0]]
# initial = [[1, 0, 2],
#            [4, 5, 3], 
#            [7, 8, 6]]

goal = [[1, 2, 3], 
        [4, 5, 6], 
        [7, 8, 0]]
puzzle = Puzzle8(initial)
path = puzzle.bfs(goal)

if path:
    for step in path:
        for row in step:
            print(row)
        print("-----")
else:
    print("No se encontró solución.")

end_time = time.time()  # Guarda el tiempo final
elapsed_time = end_time - start_time  # Calcula el tiempo transcurrido
print(f"El código tardó {round(elapsed_time, 1)} segundos en ejecutarse.")