class Nodo():
    padre=None
    hijos=None
    dato=None
    
    #this == self 
    # Esta función es el contructor
    def __init__(self,dato,hijos=None):
        self.hijos=None
        self.padre=None
        self.dato=dato
        self.asignar_hijos(hijos)
    
    # Obtener datos
    def obtener_datos(self):
        return self.dato
    
    # Asignar dato
    def asignar_datos(self,dato):
        self.dato=dato

    # Asignar hijos
    def asignar_hijos(self,hijos):
        self.hijos=hijos
        if self.hijos!=None:
            for hijito in self.hijos:
                hijito.padre=self
    # Obtener hijos
    def obtener_hijos(self):
        return self.hijos

    # Obtener padre
    def obtener_padre(self):
        return self.padre
    
    # Asignar Padre
    def asignar_padre(self,padre):
        self.padre=padre


    def igual(self,nodo):
        if self.obtener_datos()==nodo.obtener_datos():
            return True
        else:
            return False

    def en_lista(self,lista_nodos):
        en_la_lista=False
        for n in lista_nodos:
            if self.igual(n):
                en_la_lista=True


    def __str__(self):        
        return str(self.obtener_datos())
    


    

