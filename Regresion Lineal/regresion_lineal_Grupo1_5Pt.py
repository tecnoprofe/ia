import numpy as np 
import matplotlib.pyplot as plt 
import tensorflow as tf
from tensorflow._api.v2 import train
from tensorflow.python.client import session
from tensorflow.python.ops.gen_array_ops import Shape

# libreria Scikit-learn
from sklearn import datasets
from tensorflow.python.framework import ops
ops.reset_default_graph()
tf.compat.v1.disable_eager_execution()
session = tf.compat.v1.Session() 

iris = datasets.load_iris()
xval = np.array([x[3] for x in iris.data])
yval = np.array([y[0] for y in iris.data])

batch_size = 25

# marcador de posision
x_data = tf.compat.v1.placeholder(dtype=tf.float32, shape=[None,1])
y_target = tf.compat.v1.placeholder(dtype=tf.float32, shape=[None,1])

# declaracion de variables para la regresion lineal
a = tf.Variable(tf.compat.v1.random_normal(shape=[1,1]))
b = tf.Variable(tf.compat.v1.random_normal(shape=[1,1]))

# calculo de la funcion y= ax + b
calculo = tf.add(tf.matmul(x_data,a),b)

# funcion loss
loss = tf.reduce_mean(tf.square(y_target - calculo))

myopt = tf.compat.v1.train.GradientDescentOptimizer(0.05)  #ese
train_step = myopt.minimize(loss)

init = tf.compat.v1.initialize_all_variables()
session.run(init)

loss_vec = []
for i in range(100):
    rand_index = np.random.choice(len(xval), size=batch_size)
    rand_x = np.transpose([xval[rand_index]])
    rand_y = np.transpose([yval[rand_index]])
    session.run(train_step, feed_dict ={x_data: rand_x, y_target: rand_y})
    temp_loss = session.run(loss, feed_dict={x_data: rand_x, y_target: rand_y})
    loss_vec.append(temp_loss)
    if (i+1)%25==0:
        [pendiente] = session.run(a)
        [y_intercept] = session.run(b)

mejor_linea =[]
for i in xval:
    mejor_linea.append(pendiente*i+y_intercept)

plt.plot(xval, yval, 'o', label='Puntos de Datos')
plt.plot(xval,mejor_linea, 'r-', label='Mejor linea de Ajuste', linewidth=3 )
plt.legend(loc='upper left')
plt.title('Regresion Lineal')
plt.show()
    



