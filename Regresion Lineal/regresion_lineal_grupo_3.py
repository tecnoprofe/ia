import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets,linear_model
from sklearn.model_selection import train_test_split
#cargamos los datos de la DB de sklearn-boston
boston = datasets.load_boston()
#selecciona la columna 5 de la DB
x = boston.data[:,np.newaxis,5]
#difine los datos correspondiente a la etiqueta
y = boston.target
#los datos de entrenamiento y prueba
xtrain ,xtest ,ytrain , ytest =train_test_split(x,y,test_size = 0.7)
#creamos la linea de regresion lineal
lineaR = linear_model.LinearRegression()
#ponemos los datos de entrenamiento a entrenar
lineaR.fit(xtrain,ytrain)
#realiza un prediccion cpn los datos de prueba
y_predic = lineaR.predict(xtest)
print(boston)
#graficamos los puntos de dispersion
plt.scatter(xtest,ytest)
#graficamos la linea de prediccion
plt.plot(xtest,y_predic,color="red",linewidth=3)
##########complemento del grafico
plt.title("prueba regresion lineal")
plt.xlabel("numero de habitaciones ")
plt.ylabel("valor medio ")
plt.show()


