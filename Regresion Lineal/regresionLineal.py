import  matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
x_data = np.linspace(-1,1,100)
np.random.seed(5) 
y_data = 2 * x_data + 1.0 + np.random.randn(100) * 0.4 
plt.xlabel('x')
plt.ylabel('y')
plt.title('Datos de entrenamiento')
plt.scatter(x_data,y_data)
plt.plot(x_data,  1.0 + 2 * x_data ,'r' ,linewidth=3)
plt.show()

#y = 2x + 1