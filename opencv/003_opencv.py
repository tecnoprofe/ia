# CONVERTIR UN TOMA DE VIDEO A ESCALA DE GRISES.

# importar la libreria OpenCV
import cv2
# capturar una camara
camara = cv2.VideoCapture(0)
while True:
    # un frame es una imagen en un determinado milisegundo
    ret,frame = camara.read()
    # Convertir a escala de grises
    frame_gris=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)        
    # mostrar foto video real y en gris
    cv2.imshow('frame', frame_gris)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

camara.release()
cv2.destroyAllWindows()