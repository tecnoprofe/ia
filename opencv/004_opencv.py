# CONVERTIR UN TOMA DE VIDEO A ESCALA DE GRISES.
# importar la libreria OpenCV
import cv2
# capturar una camara
camara = cv2.VideoCapture(0)

#clasificador de ojos
clasificador=cv2.CascadeClassifier('C:/tecnoprofe/ia/opencv/haarcascade_eye.xml')

while True:    
    # un frame es una imagen en un determinado milisegundo
    ret,frame = camara.read()
    # Convertir a escala de grises
    frame_gris=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    #Detectar los ojos
    deteccion=clasificador.detectMultiScale(frame_gris,1.5,5)
    for (x,y,w,h) in deteccion:
        cv2.line(frame_gris,(x,y),(x+w,y+h),(255,255,255),5)
        #remplazar una imagen de lente.

    # mostrar foto video real
    cv2.imshow('frame1', frame)    
    
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

camara.release()
cv2.destroyAllWindows()
